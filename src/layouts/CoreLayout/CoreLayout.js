import React from 'react'
import Header from '../../components/Header'
import AppRibbon from '../../components/AppRibbon'
import Breadcrubms from '../../components/Breadcrumbs'
import './CoreLayout.scss'
import '../../styles/core.scss'

export const CoreLayout = ({ children }) => (
  <div className='container'>
    <Header />
    <AppRibbon />
    <div className='inner-container clearfix'>
      <Breadcrubms />
      <div className='app-container'>
        <div className='panel panel-default'>
          <div className='panel-body'>
            {children}
          </div>
        </div>
      </div>
    </div>
  </div>
)

CoreLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default CoreLayout
