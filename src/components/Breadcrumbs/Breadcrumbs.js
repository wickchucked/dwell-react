import React from 'react'
import './Breadcrumbs.scss'

export const Breadcrumbs = () => (
  <ol className='breadcrumb hidden-print'>
    <li className='active'>Dwelling Fire Menu</li>
  </ol>
)

export default Breadcrumbs
