import React from 'react'
import './Header.scss'

export const Header = () => (
  <div className='navbar pg-header'>
    <div className='navbar-inner'>
      <div className='col-6'>
        <h1 className='brand'>
          <a href='#' title='Employee Menu'>
            <img src='NSM-Logo-web.gif' alt='North Star Mutual Insurance Company' className='img-responsive' />
          </a>
        </h1>
      </div>
      <div className='col-6'>
        <h1 className='pg-title'>
          Dwelling Fire Quoting
        </h1>
      </div>
    </div>
  </div>
)

export default Header
