import React from 'react'
import './AppRibbon.scss'

export const AppRibbon = () => (
  <div className='navbar navbar-default app-ribbon'>
    <div className='navbar-header'>
      <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
        <span className='icon-bar' />
        <span className='icon-bar' />
        <span className='icon-bar' />
      </button>
      <a className='navbar-brand' href='#'>
        <span className='glyphicon glyphicon-home' />
        &nbsp; Employee Menu
      </a>
    </div>
  </div>
)

export default AppRibbon
